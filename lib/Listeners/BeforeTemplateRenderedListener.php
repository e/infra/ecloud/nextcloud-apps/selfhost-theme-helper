<?php

namespace OCA\SelfhostThemeHelper\Listeners;

use OCP\EventDispatcher\Event;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use \OCP\EventDispatcher\IEventListener;
use OCP\Util;
use OCP\IUserSession;
use OCP\IRequest;
use OCP\App\IAppManager;

class BeforeTemplateRenderedListener implements IEventListener {
	private Util $util;
	private IUserSession $userSession;
	private IRequest $request;
	private string $appName;
	private IAppManager $appManager;

	public function __construct($appName, Util $util, IUserSession $userSession, IRequest $request, IAppManager $appManager) {
		$this->appName = $appName;
		$this->util = $util;
		$this->userSession = $userSession;
		$this->request = $request;
		$this->appManager = $appManager;
	}
	public function handle(Event $event): void {
		if (!($event instanceof BeforeTemplateRenderedEvent)) {
			return;
		}
		$pathInfo = $this->request->getPathInfo();

		if (!$this->userSession->isLoggedIn()) {
			$this->util->addStyle($this->appName, 'login');
		} else {
			$this->util->addStyle($this->appName, 'header');
		}
	}
}
